import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Pelicula } from './pelicula';
import { map, first } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';



@Injectable({
  providedIn: 'root'
})
export class PeliculaService {
  private endPoint:string;
  pelicula:Pelicula;
  peliculaAux:Observable<Pelicula>;
  constructor(private _http: HttpClient,){}
 
  buscar(nombre:string): any{  
    this.endPoint= "http://localhost:8081/buscador/";
      this.endPoint = this.endPoint.concat(nombre);  
      pelicula:Pelicula;
      console.log("estamos pasando por el servicio con este endpoit",this.endPoint)
     return this._http.get(this.endPoint);
     
    }

    inicio(): Observable<any>{  
      this.endPoint= "http://localhost:8081/buscar/inicio";  
      pelicula:Pelicula;
      console.log("estamos pasando por el servicio con este endpoit",this.endPoint)
     return this._http.get(this.endPoint);
     
    }

    busquedaCompleta(nombre:string): any{
      this.endPoint= "http://localhost:8081/busquedaCompleta/";  
      this.endPoint = this.endPoint.concat(nombre);  
      pelicula:Pelicula;
      console.log("estamos pasando por el servicio con este endpoit",this.endPoint)
     return this._http.get(this.endPoint);

    }

    buscarConcreta(nombre:string): any{  
      this.endPoint= "http://localhost:8081//buscador-concreto/";
        this.endPoint = this.endPoint.concat(nombre);  
    
        console.log("estamos pasando por el servicio con este endpoit",this.endPoint)
       return this._http.get(this.endPoint);
       
      }

      buscarRecomendadas(nombre:string): any{  
        this.endPoint= "http://localhost:8081//buscador-reomendadas/";
          this.endPoint = this.endPoint.concat(nombre);  
        
          console.log("estamos pasando por el servicio con este endpoit",this.endPoint)
         return this._http.get(this.endPoint);
         
        }

}
