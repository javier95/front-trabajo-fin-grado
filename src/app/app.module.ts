import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { APP_ROUTING } from './app.routes';
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';

import { HttpClientModule } from '@angular/common/http';
import { PeliculaComponent } from './components/pelicula/pelicula.component';
import { PeliculaService } from './service/pelicula/pelicula.service';
import { BuscadorComponent } from './components/buscador/buscador.component';
import { HomeComponent } from './components/home/home.component';
import { PeliculaInfoComponent } from './components/pelicula-info/pelicula-info.component';



@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    BuscadorComponent,
    PeliculaComponent,
    HomeComponent,
    PeliculaInfoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    APP_ROUTING
    
  ],
  providers: [PeliculaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
