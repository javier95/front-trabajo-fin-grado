import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PeliculaService } from 'src/app/service/pelicula/pelicula.service';
import { Pelicula } from 'src/app/service/pelicula/pelicula';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
  _pelicula:Array<Pelicula>;
  constructor(private activatedRoute:ActivatedRoute,
    private _peliculaService:PeliculaService) { }

  ngOnInit() {
    
this._peliculaService.inicio().subscribe(result =>{
  this._pelicula= result;})
  console.log("pelis",this._pelicula);
}
}
