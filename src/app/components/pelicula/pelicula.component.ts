import { Component, OnInit, Input } from '@angular/core';
import { Pelicula } from 'src/app/service/pelicula/pelicula';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pelicula',
  templateUrl: './pelicula.component.html',
  styleUrls: ['./pelicula.component.css']
})
export class PeliculaComponent implements OnInit {

  @Input() pelicula:Pelicula;
  constructor(private router:Router) { }

  ngOnInit() {
  }

  buscar(nombre:string){
    console.log("esto es el header")
    this.router.navigate( ['/info',nombre] );
   //this._servicePelicula.buscar(nombre).subscribe(pelicula => this._pelicula = pelicula);
   //console.log("esto es la pelicula", this._pelicula)
  }

}
