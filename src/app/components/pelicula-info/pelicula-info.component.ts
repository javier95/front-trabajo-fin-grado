import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PeliculaService } from 'src/app/service/pelicula/pelicula.service';
import { Pelicula } from 'src/app/service/pelicula/pelicula';

@Component({
  selector: 'app-pelicula-info',
  templateUrl: './pelicula-info.component.html'
  
})
export class PeliculaInfoComponent implements OnInit {

  _pelicula:Pelicula[];
  _peliculasRecomendadas:Pelicula[];
  nombre:string;
  
  constructor(private activatedRoute:ActivatedRoute,
    private _peliculaService:PeliculaService) { }

  ngOnInit() {

    this.activatedRoute.params.subscribe( params =>{
      this.nombre =params['nombre'];
      this._peliculaService.buscarConcreta( params['nombre'] ).subscribe(result =>{
        console.log("Y esto es lo que ha funcionanado anda la verga",result);
        this._pelicula= result;       
        console.log("Estamos de vuelta en el becador component con la pelicula", this._pelicula) 
      });
      this._peliculaService.buscarRecomendadas( params['nombre'] ).subscribe(resultado =>{
        console.log("Y esto es lo que ha funcionanado anda la verga",resultado);
        this._peliculasRecomendadas= resultado;       
        console.log("Estamos de vuelta en el becador component con la pelicula Recomended", this._pelicula) 
      });
    });
  }

}
