import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PeliculaService } from 'src/app/service/pelicula/pelicula.service';
import { Pelicula } from 'src/app/service/pelicula/pelicula';
import {Observable } from 'rxjs';


@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html'
})
export class BuscadorComponent implements OnInit {

  _pelicula:Pelicula[];
  nombre:string;

  constructor( private activatedRoute:ActivatedRoute,
              private _peliculaService:PeliculaService) {

  }

  ngOnInit() {

    this.activatedRoute.params.subscribe( params =>{
      this.nombre =params['nombre'];
      this._peliculaService.buscar( params['nombre'] ).subscribe(result =>{
        console.log("Y esto es lo que ha funcionanado anda la verga",result);
        this._pelicula= result;       
        console.log("Estamos de vuelta en el becador component con la pelicula", this._pelicula) 
      });
      
    });
    
  }

}
