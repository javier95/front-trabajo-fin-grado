import { RouterModule, Routes } from '@angular/router';
import { BuscadorComponent } from './components/buscador/buscador.component';

import { HomeComponent } from './components/home/home.component';
import { PeliculaInfoComponent } from './components/pelicula-info/pelicula-info.component';

const APP_ROUTES: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'info/:nombre', component: PeliculaInfoComponent },
  { path: 'buscador/:nombre', component: BuscadorComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, {useHash:true});
